# Codewars katas in JS, with tests in Chai and Mocha

See my [Codewars profile][profile] for which katas I've solved.

[profile]: https://www.codewars.com/users/ian-s-mcb/completed
