// Linked Lists - Push & BuildOneTwoThree
// http://www.codewars.com/kata/linked-lists-push-and-buildonetwothree
function Node (data, next = null) {
  this.data = data
  this.next = next
}
let push = (head, data) => new Node(data, head)
let build = (...args) => args.reduce(push, null)
let buildOneTwoThree = build.bind(null, 3, 2, 1)

export default { Node, push, build, buildOneTwoThree }
