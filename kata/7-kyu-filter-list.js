// List Filtering
// https://www.codewars.com/kata/53dbd5315a3c69eed20002dd
function filter_list (l) { // eslint-disable-line camelcase
  return l.filter(x => typeof x === 'number')
}

export default filter_list // eslint-disable-line camelcase
