// Linked Lists - Remove duplicates
// http://www.codewars.com/kata/linked-lists-remove-duplicates
import linkedListPushBuild from './7-kyu-linked-lists-push-build'
let Node = linkedListPushBuild.Node

function removeDuplicates (head) {
  let current = head
  let prev = new Node()
  while (current !== null) {
    if (prev.data === current.data) {
      prev.next = current.next
    } else {
      prev = current
    }
    current = current.next
  }
  return head
}

export default removeDuplicates
