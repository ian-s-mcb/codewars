'use strict'

import formatDuration from './4-kyu-format-duration'
import romanNumeralsDecoder from './4-kyu-roman-numerals-decoder'
import romanNumeralsEncoder from './4-kyu-roman-numerals-encoder'
import extractDomainName from './5-kyu-extract-domain-name'
import firstNonRepeatingLetter from './5-kyu-first-non-repeating-letter'
import landPerimeter from './5-kyu-land-perimeter'
import largest5DigitNumber from './5-kyu-largest-5-digit-number'
import linkedListsAlternatingSplit from './5-kyu-linked-lists-alternating-split'
import linkedListsFrontBackSplit from './5-kyu-linked-lists-front-back-split'
import moveZeros from './5-kyu-move-zeros'
import PaginationHelper from './5-kyu-pagination-helper'
import SnakesLadders from './5-kyu-snakes-and-ladders'
import alphabetPosition from './6-kyu-alphabet-position'
import countBits from './6-kyu-count-bits'
import decodeMorse from './6-kyu-decode-morse'
import digPow from './6-kyu-digit-power'
import iqTest from './6-kyu-iq-test'
import linkedListsIterativeReverse from './6-kyu-linked-lists-iterative-reverse'
import linkedListsLengthCount from './6-kyu-linked-lists-length-count'
import linkedListsMergeSort from './6-kyu-linked-lists-merge-sort'
import linkedListsRecursiveReverse from './6-kyu-linked-lists-recursive-reverse'
import linkedListsRemoveDuplicates from './6-kyu-linked-lists-remove-duplicates'
import linkedListsShuffleMerge from './6-kyu-linked-lists-shuffle-merge'
import linkedListsSortedInsert from './6-kyu-linked-lists-sorted-insert'
import linkedListsSortedIntersect from './6-kyu-linked-lists-sorted-intersect'
import linkedListsSortedMerge from './6-kyu-linked-lists-sorted-merge'
import longestConsec from './6-kyu-longest-consecutive'
import tickets from './6-kyu-vasya-clerk'
import descendingOrder from './7-kyu-descending-order'
import filterList from './7-kyu-filter-list'
import linkedListsAppend from './7-kyu-linked-lists-append'
import linkedListsMoveNode from './7-kyu-linked-lists-move-node'
import linkedListsGetNthNode from './7-kyu-linked-lists-get-nth-node'
import linkedListsPushBuild from './7-kyu-linked-lists-push-build'
import multiply from './8-kyu-multiply'
import linkedListsMoveNodeInPlace from './beta-linked-lists-move-node-in-place'

export default {
  formatDuration,
  romanNumeralsDecoder,
  romanNumeralsEncoder,
  extractDomainName,
  firstNonRepeatingLetter,
  landPerimeter,
  largest5DigitNumber,
  linkedListsAlternatingSplit,
  linkedListsFrontBackSplit,
  moveZeros,
  PaginationHelper,
  SnakesLadders,
  alphabetPosition,
  countBits,
  decodeMorse,
  digPow,
  iqTest,
  linkedListsIterativeReverse,
  linkedListsLengthCount,
  linkedListsMergeSort,
  linkedListsRecursiveReverse,
  linkedListsRemoveDuplicates,
  linkedListsShuffleMerge,
  linkedListsSortedInsert,
  linkedListsSortedIntersect,
  linkedListsSortedMerge,
  longestConsec,
  tickets,
  descendingOrder,
  filterList,
  linkedListsAppend,
  linkedListsMoveNode,
  linkedListsGetNthNode,
  linkedListsPushBuild,
  multiply,
  linkedListsMoveNodeInPlace
}
