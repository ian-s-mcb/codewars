// Consecutive strings
// https://www.codewars.com/kata/56a5d994ac971f1ac500003e
function longestConsec (strarr, k) {
  let longest = ''
  let str
  if (k < 1) return longest
  for (let i = 0; i <= (strarr.length - k); i++) {
    str = strarr.slice(i, i + k).join('')
    if (str.length > longest.length) longest = str
  }
  return longest
}

export default longestConsec
