// Linked Lists Merge Sort
// http://www.codewars.com/kata/linked-lists-merge-sort
import pushBuild from './7-kyu-linked-lists-push-build'
import frontBackSplit from './5-kyu-linked-lists-front-back-split'
import sortedMerge from './6-kyu-linked-lists-sorted-merge'
let Node = pushBuild.Node

function mergeSort (list) {
  if (!list || !list.next) return list

  let front = new Node()
  let back = new Node()
  frontBackSplit(list, front, back)
  return sortedMerge(mergeSort(front), mergeSort(back))
}

export default mergeSort
