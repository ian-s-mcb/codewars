// Linked Lists Sorted Intersect
// http://www.codewars.com/kata/linked-lists-sorted-intersect
import pushBuild from './7-kyu-linked-lists-push-build'
let Node = pushBuild.Node

function sortedIntersect (first, second) {
  let next
  if (!first || !second) return null
  else if (first.data === second.data) {
    next = sortedIntersect(first.next, second.next)
    return (!next || (first.data !== next.data))
      ? new Node(first.data, next)
      : next
  } else {
    return (first.data > second.data)
      ? sortedIntersect(first, second.next)
      : sortedIntersect(first.next, second)
  }
}
export default sortedIntersect
