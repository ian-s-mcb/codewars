// IQ Test
// https://www.codewars.com/kata/552c028c030765286c00007d
function iqTest (numbers) {
  let arr = numbers.split(' ').map(x => parseInt(x) % 2)
  let partialSum = arr[0] + arr[1] + arr[2]
  let target = partialSum < 2 ? 1 : 0

  return arr.indexOf(target) + 1
}

export default iqTest
