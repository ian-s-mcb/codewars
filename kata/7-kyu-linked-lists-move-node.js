// Linked Lists - Move Node
// http://www.codewars.com/kata/linked-lists-move-node
function Context (source, dest) {
  this.source = source
  this.dest = dest
}

function moveNode (source, dest) {
  if (!source) {
    throw new Error('No source node available for moving')
  }
  let sourceOld = source
  source = source.next
  sourceOld.next = dest
  dest = sourceOld
  return new Context(source, dest)
}

export default moveNode
