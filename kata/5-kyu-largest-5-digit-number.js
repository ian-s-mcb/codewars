// Largest 5 digit number in a series
// https://www.codewars.com/kata/51675d17e0c1bed195000001
function solution (digits) {
  var max = 0
  for (let i = 0; i < (digits.length - 4); i++) {
    max = Math.max(max, +digits.substring(i, i + 5))
  }
  return max
}

export default solution
