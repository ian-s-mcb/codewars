// Replace With Alphabet Position
// https://www.codewars.com/kata/546f922b54af40e1e90001da
function alphabetPosition (text) {
  let offset = 'a'.charCodeAt(0) - 1
  return text.toLowerCase().replace(/[^a-z]/g, '').split('').map(x => x.charCodeAt(0) - offset).join(' ')
}

export default alphabetPosition
