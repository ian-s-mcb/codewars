// Linked Lists - Get Nth Node
// http://www.codewars.com/kata/linked-lists-get-nth-node
function getNth (node, index) {
  var current = node
  if (index < 0) throw new Error('ArgumentException')
  for (let i = index; i > 0 && current != null; i--) {
    current = current.next
  }
  if (current === null) throw new Error('ArgumentException')
  return current
}

export default getNth
