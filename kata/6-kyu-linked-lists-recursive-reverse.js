// Linked Lists Recursive Reverse
// http://www.codewars.com/kata/linked-lists-recursive-reverse
function reverse (list) {
  function helper (head, reversed) {
    if (!head) return reversed
    let newHead = head.next
    head.next = reversed
    return helper(newHead, head)
  }
  return list ? helper(list, null) : null
}

export default reverse
