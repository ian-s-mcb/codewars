// Linked Lists Front Back Split
// http://www.codewars.com/kata/linked-lists-front-back-split
function frontBackSplit (source, front, back) {
  let slow
  let fast
  if (!source || !source.next) {
    throw new Error('Cannot split list with few than two nodes')
  }
  front.data = source.data
  if (!source.next.next) front.next = null
  else front.next = source.next
  fast = slow = source
  while (fast && fast.next && fast.next.next) {
    slow = slow.next
    fast = fast.next.next
  }
  back.data = slow.next.data
  back.next = slow.next.next
  slow.next = null
}

export default frontBackSplit
