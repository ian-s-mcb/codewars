// Linked Lists Alternating Split
// http://www.codewars.com/kata/linked-lists-alternating-split
function Context (first, second) {
  this.first = first
  this.second = second
}

function alternatingSplit (head) {
  let alternate = true
  let currentF
  let currentS
  let result
  if (!head || !head.next) {
    throw new Error('Input list must have at least two nodes')
  }
  currentF = head
  currentS = head.next
  result = new Context()
  result.first = currentF
  result.second = currentS
  while (currentF && currentS) {
    if (alternate) {
      currentF.next = currentS.next
      currentF = currentF.next
    } else {
      currentS.next = currentF.next
      currentS = currentS.next
    }
    alternate = !alternate
  }
  return result
}

export default { alternatingSplit, Context }
