// Linked Lists - Append
// http://www.codewars.com/kata/linked-lists-append
function append (listA, listB) {
  if (listA) {
    let current = listA
    let prev = null
    while (current !== null) {
      prev = current
      current = current.next
    }
    prev.next = listB
    return listA
  } else {
    return listB
  }
}

export default append
