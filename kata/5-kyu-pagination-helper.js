// PaginationHelper
// https://www.codewars.com/kata/paginationhelper
function PaginationHelper (collection, itemsPerPage) {
  this.collection = collection
  this.itemsPerPage = itemsPerPage
}

PaginationHelper.prototype.itemCount = function () {
  return this.collection.length
}

PaginationHelper.prototype.pageCount = function () {
  return Math.ceil(this.collection.length / this.itemsPerPage)
}

PaginationHelper.prototype.pageItemCount = function (pageIndex) {
  return pageIndex < this.pageCount()
    ? Math.min(this.itemsPerPage, this.collection.length - (pageIndex * this.itemsPerPage))
    : -1
}

PaginationHelper.prototype.pageIndex = function (itemIndex) {
  return ((itemIndex >= 0) && (itemIndex < this.collection.length))
    ? Math.floor(itemIndex / this.itemsPerPage)
    : -1
}

export default PaginationHelper
