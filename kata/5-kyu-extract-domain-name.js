// Extract the domain name from a URL
// https://www.codewars.com/kata/extract-the-domain-name-from-a-url-1
function domainName (url) {
  return url.replace(/https?:\/\/|www\./g, '').split('.')[0]
}

export default domainName
