// Playing with digits
// https://www.codewars.com/kata/5552101f47fc5178b1000050
function digPow (n, p) {
  let x = n.toString().split('').reduce((s, d, i) => s + (d ** (p + i)), 0)
  return x % n ? -1 : x / n
}

export default digPow
