// Linked Lists Iterative Reverse
// http://www.codewars.com/kata/linked-lists-iterative-reverse
function reverse (list) {
  let current = list
  let previous = null
  let backup
  while (current) {
    backup = current.next
    current.next = previous
    previous = current
    current = backup
  }
  return previous
}
export default reverse
