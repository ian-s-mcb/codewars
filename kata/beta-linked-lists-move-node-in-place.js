// Linked Lists - Move Node In-Place
// http://www.codewars.com/kata/linked-lists-move-node-in-place
import linkedListsPushBuild from './7-kyu-linked-lists-push-build.js'
let Node = linkedListsPushBuild.Node

function moveNode (source, dest) {
  let oldDest
  if (!source || !source.data || !dest) {
    throw new Error('No source node available for moving')
  }
  if (dest.data) oldDest = new Node(dest.data, dest.next)
  else oldDest = null
  dest.data = source.data
  dest.next = oldDest
  if (source.next) {
    source.data = source.next.data
    source.next = source.next.next
  } else {
    source.next = source.data = null
  }
}

export default moveNode
