// Human readable duration format
// https://www.codewars.com/kata/human-readable-duration-format
function formatDuration (seconds) {
  if (!seconds) return 'now'
  const time = {
    year: 365 * 24 * 60 * 60,
    day: 24 * 60 * 60,
    hour: 60 * 60,
    minute: 60,
    second: 1
  }
  var res = []
  var val
  for (var key in time) {
    val = Math.floor(seconds / time[key])
    seconds -= val * time[key]
    if (val) res.push(`${val} ${key}${(val > 1) ? 's' : ''}`)
  }
  return res.join(', ').replace(/,([^,]*)$/, ' and$1')
}

export default formatDuration
