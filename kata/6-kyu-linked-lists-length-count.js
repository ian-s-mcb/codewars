// Linked Lists - Length & Count
// http://www.codewars.com/kata/linked-lists-length-and-count
function length (head) {
  let l = 0
  while (head != null) {
    l += 1
    head = head.next
  }
  return l
}

function count (head, data) {
  let c = 0
  while (head != null) {
    c += head.data === data
    head = head.next
  }
  return c
}

export default {length, count}
