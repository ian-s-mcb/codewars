// Linked Lists Shuffle Merge
// http://www.codewars.com/kata/linked-lists-shuffle-merge
import pushBuild from './7-kyu-linked-lists-push-build'
let Node = pushBuild.Node

function shuffleMerge (first, second) {
  if (!first) return second
  if (!second) return first
  return new Node(first.data, shuffleMerge(second, first.next))
}

export default shuffleMerge
