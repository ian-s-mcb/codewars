// Linked Lists - Sorted Insert
// http://www.codewars.com/kata/linked-lists-sorted-insert
import linkedListsPushBuild from './7-kyu-linked-lists-push-build.js'
let Node = linkedListsPushBuild.Node

function sortedInsert (head, data) {
  let prev = null
  let current = head
  let n
  while (current !== null && current.data < data) {
    prev = current
    current = current.next
  }

  n = new Node(data, current)
  if (current === head) {
    head = n
  } else {
    prev.next = n
  }

  return head
}

export default sortedInsert
