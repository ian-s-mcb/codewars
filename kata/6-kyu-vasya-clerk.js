// Vasya - Clerk
// https://www.codewars.com/kata/555615a77ebc7c2c8a0000b8
function tickets (ppl) {
  let r = {'25': 0, '50': 0}
  for (let i = 0; i < ppl.length; i++) {
    switch (ppl[i]) {
      case 25: r[25]++; break
      case 50: r[50]++; r[25]--; break
      case 100: r[25]--; r[50] ? r[50]-- : r[25] -= 2; break
    }
    if ((r[25] < 0) || (r[50] < 0)) return 'NO'
  }
  return 'YES'
}

export default tickets
