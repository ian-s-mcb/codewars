// Land Perimeter
// https://www.codewars.com/kata/land-perimeter
function landPerimeter (arr) {
  const h = arr.length - 1
  const w = arr[0].length - 1
  var p = 0
  var i, j
  for (i = 0; i <= h; i++) {
    for (j = 0; j <= w; j++) {
      if (arr[i][j] === 'X') {
        p += ((i === 0) || (arr[i - 1][j] === 'O'))
        p += ((j === w) || (arr[i][j + 1] === 'O'))
        p += ((i === h) || (arr[i + 1][j] === 'O'))
        p += ((j === 0) || (arr[i][j - 1] === 'O'))
      }
    }
  }
  return `Total land perimeter: ${p}`
}

export default landPerimeter
