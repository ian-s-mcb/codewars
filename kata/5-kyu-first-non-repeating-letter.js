// First non-repeating character
// https://www.codewars.com/kata/52bc74d4ac05d0945d00054e
function firstNonRepeatingLetter (s) {
  var counter = {}
  for (let c of s.toLowerCase()) counter[c] = (counter[c] || 0) + 1
  for (let c of s) if (counter[c.toLowerCase()] === 1) return c
  return ''
}

export default firstNonRepeatingLetter
