import { describe, expect, fail, test } from 'vitest'

import kata from '../kata'

describe('formatDuration()', function () {
  test('should handle simple tests', function () {
    expect(kata.formatDuration(1)).toBe('1 second')
    expect(kata.formatDuration(62)).toBe('1 minute and 2 seconds')
    expect(kata.formatDuration(120)).toBe('2 minutes')
    expect(kata.formatDuration(3600)).toBe('1 hour')
    expect(kata.formatDuration(3662)).toBe('1 hour, 1 minute and 2 seconds')
  })

  test('should handle a duration of zero seconds', function () {
    expect(kata.formatDuration(0)).toBe('now')
  })
})

describe('romanNumeralsDecoder()', function () {
  test('should handle simple tests', function () {
    expect(kata.romanNumeralsDecoder('MCMXC')).toBe(1990)
    expect(kata.romanNumeralsDecoder('MMVIII')).toBe(2008)
    expect(kata.romanNumeralsDecoder('MDCLXVI')).toBe(1666)
  })

  test('should handle some big numbers', function () {
    expect(kata.romanNumeralsDecoder('MMMDCCCLXXXVIII')).toBe(3888)
    expect(kata.romanNumeralsDecoder('MMMMMDCCXLVIII')).toBe(5748)
    expect(kata.romanNumeralsDecoder('MMMMMMMMMXLV')).toBe(9045)
  })
})

describe('romanNumeralsEncoder()', function () {
  test('should handle simple tests', function () {
    expect(kata.romanNumeralsEncoder(1990)).toBe('MCMXC')
    expect(kata.romanNumeralsEncoder(2008)).toBe('MMVIII')
    expect(kata.romanNumeralsEncoder(1666)).toBe('MDCLXVI')
  })

  test('should handle some big numbers', function () {
    expect(kata.romanNumeralsEncoder('3888')).toBe('MMMDCCCLXXXVIII')
    expect(kata.romanNumeralsEncoder('5748')).toBe('MMMMMDCCXLVIII')
    expect(kata.romanNumeralsEncoder('9045')).toBe('MMMMMMMMMXLV')
  })
})

describe('extractDomainName', function () {
  var cases = [
    {args: ['http://google.com'], expected: 'google'},
    {args: ['http://google.co.jp'], expected: 'google'},
    {args: ['www.xakep.ru'], expected: 'xakep'},
    {args: ['https://youtube.com'], expected: 'youtube'},
    {args: ['icann.org'], expected: 'icann'},
    {args: ['https://www.2dn2xhz06zpsyb.co.za/archive/'], expected: '2dn2xhz06zpsyb'}
  ]

  cases.forEach(function (c) {
    test(`should extract the domain in ${c.args[0]}`, function () {
      var res = kata.extractDomainName.apply(null, c.args)
      expect(res).toBe(c.expected)
    })
  })
})

describe('firstNonRepeatingLetter()', function () {
  test('should handle simple tests', function () {
    expect(kata.firstNonRepeatingLetter('a')).toBe('a')
    expect(kata.firstNonRepeatingLetter('stress')).toBe('t')
    expect(kata.firstNonRepeatingLetter('moonmen')).toBe('e')
  })

  test('should handle some upper and lower case letters', function () {
    expect(kata.firstNonRepeatingLetter('sTreSS')).toBe('T')
    expect(kata.firstNonRepeatingLetter('sTreSSt')).toBe('r')
  })
})

describe('landPerimeter()', function () {
  test('should handle a 3x3 map', function () {
    expect(kata.landPerimeter(
      [
        'XOO',
        'XOX',
        'XOX'
      ]
    )).toBe('Total land perimeter: 14')
  })

  test('should handle a 4x5 map', function () {
    expect(kata.landPerimeter(
      [
        'XOOO',
        'XOXO',
        'XOXO',
        'OOXX',
        'OOOO'
      ]
    )).toBe('Total land perimeter: 18')
  })

  test('should handle a 5x5 map', function () {
    expect(kata.landPerimeter(
      [
        'XOOXO',
        'XOOXO',
        'OOOXO',
        'XXOXO',
        'OXOOO'
      ]
    )).toBe('Total land perimeter: 24')
  })

  test('should handle a 6x10 map', function () {
    expect(kata.landPerimeter(
      [
        'OXOOOX',
        'OXOXOO',
        'XXOOOX',
        'OXXXOO',
        'OOXOOX',
        'OXOOOO',
        'OOXOOX',
        'OOXOOO',
        'OXOOOO',
        'OXOOXX'
      ]
    )).toBe('Total land perimeter: 60')
  })
})

describe('largest5DigitNumber()', function () {
  var cases = [
    {
      description: '10 digit input',
      args: ['1234567890'],
      expected: 67890
    },
    {
      description: '15 digit input',
      args: ['881203994567890'],
      expected: 99456
    }
  ]

  cases.forEach(function (c) {
    test(`should find max in a ${c}`, function () {
      var res = kata.largest5DigitNumber.apply(null, c.args)
      expect(res).toBe(c.expected)
    })
  })
})

describe('moveZeros()', function () {
  var cases = [
    {
      description: 'input consisting of ints',
      args: [[0, 5, 0]],
      expected: [5, 0, 0]
    },
    {
      description: 'input consisting of no zeros',
      args: [[1, 5, 2]],
      expected: [1, 5, 2]
    },
    {
      description: 'input consisting of booleans, strings, and ints',
      args: [[false, 1, 0, 1, 2, 0, 1, 3, 'a']],
      expected: [false, 1, 1, 2, 1, 3, 'a', 0, 0]
    }
  ]

  cases.forEach(function (c) {
    test(`should move zeros in an ${test.description}`, function () {
      var res = kata.moveZeros.apply(null, c.args)
      expect(res).toEqual(c.expected)
    })
  })
})

describe('PaginationHelper', function () {
  test('should handle simple tests', function () {
    var helper = new kata.PaginationHelper(['a', 'b', 'c', 'd', 'e', 'f'], 4)

    expect(helper.pageCount()).toBe(2)

    expect(helper.itemCount()).toBe(6)

    expect(helper.pageItemCount(0)).toBe(4)
    expect(helper.pageItemCount(1)).toBe(2)
    expect(helper.pageItemCount(2)).toBe(-1)

    expect(helper.pageIndex(5)).toBe(1)
    expect(helper.pageIndex(2)).toBe(0)
    expect(helper.pageIndex(20)).toBe(-1)
    expect(helper.pageIndex(-10)).toBe(-1)
  })
  test('should handle an empty collection', function () {
    var helper = new kata.PaginationHelper([], 3)

    expect(helper.pageCount()).toBe(0)
    expect(helper.itemCount()).toBe(0)
    expect(helper.pageItemCount(0)).toBe(-1)
    expect(helper.pageIndex(0)).toBe(-1)
  })
})

describe('SnakesLadders', function () {
  test('should handle a simple game', function () {
    var game = new kata.SnakesLadders()
    expect(game.play(1, 2)).toBe('Player 1 is on square 3')
    expect(game.play(1, 1)).toBe('Player 2 is on square 38')
    expect(game.play(6, 2)).toBe('Player 2 is on square 25')
    expect(game.play(6, 6)).toBe('Player 1 is on square 26')
    expect(game.play(1, 1)).toBe('Player 1 is on square 84')
    expect(game.play(2, 1)).toBe('Player 1 is on square 94')
    expect(game.play(2, 1)).toBe('Player 2 is on square 84')
    expect(game.play(3, 3)).toBe('Player 1 Wins!')
    expect(game.play(2, 1)).toBe('Game over!')
  })
})

describe('alphabetPosition()', function () {
  var letters = 'abcdefghijklmnopqrstuvwxyz'.split('')

  letters.forEach(function (letter, index, _) {
    test(`should converts the individual letter ${letter}`, function () {
      var res = kata.alphabetPosition(letter)
      expect(res).toBe((index + 1).toString())
    })
  })

  var longString = "The sunset sets at twelve o' clock."
  var longStringConverted = '20 8 5 19 21 14 19 5 20 19 5 20 19 1 20 20 23 5 12 22 5 15 3 12 15 3 11'
  test(`should convert the long string ${longString}`, function () {
    var res = kata.alphabetPosition(longString)
    expect(res).toBe(longStringConverted)
  })
})

describe('countBits()', function () {
  var cases = [
    {args: [0], expected: 0},
    {args: [1], expected: 1},
    {args: [4], expected: 1},
    {args: [7], expected: 3},
    {args: [9], expected: 2},
    {args: [10], expected: 2},
    {args: [1234], expected: 5}
  ]

  cases.forEach(function (c) {
    test(`should count the 1's in the binary representation of the integer ${c.args[0]}`, function () {
      var res = kata.countBits.apply(null, c.args)
      expect(res).toBe(c.expected)
    })
  })
})

describe('decodeMorse()', function () {
  var cases = [
    {args: ['.... . -.--   .--- ..- -.. .'], expected: 'HEY JUDE'}
  ]

  cases.forEach(function (c) {
    test(`should decodes ${c.args[0]}`, function () {
      var res = kata.decodeMorse.decodeMorse.apply(null, c.args)
      expect(res).toBe(c.expected)
    })
  })
})

describe('digPow()', function () {
  var cases = [
    {args: [89, 1], expected: 1},
    {args: [92, 1], expected: -1},
    {args: [695, 2], expected: 2},
    {args: [46288, 3], expected: 51}
  ]

  cases.forEach(function (c) {
    test(`should return the magic number for n=${c.args[0]},p=${c.args[1]}`, function () {
      var res = kata.digPow.apply(null, c.args)
      expect(res).toBe(c.expected)
    })
  })
})

describe('iqTest()', function () {
  var cases = [
    {args: ['2 4 7 8 10'], expected: 3},
    {args: ['1 2 1 1'], expected: 2}
  ]

  cases.forEach(function (c) {
    test(`should return the index of the number in ${c.args[0]} that has a different evenness than the other numbers`, function () {
      var res = kata.iqTest.apply(null, c.args)
      expect(res).toBe(c.expected)
    })
  })
})

describe('longestConsec()', function () {
  var cases = [
    {
      args: [
        ['zone', 'abigail', 'theta', 'form', 'libe', 'zas', 'theta', 'abigail'],
        2
      ],
      expected: 'abigailtheta'
    },
    {
      args: [
        ['ejjjjmmtthh', 'zxxuueeg', 'aanlljrrrxx', 'dqqqaaabbb', 'oocccffuucccjjjkkkjyyyeehh'],
        1
      ],
      expected: 'oocccffuucccjjjkkkjyyyeehh'
    },
    {
      args: [
        [],
        3
      ],
      expected: ''
    },
    {
      args: [
        ['it', 'wkppv', 'ixoyx', '3452', 'zzzzzzzzzzzz'],
        0
      ],
      expected: ''
    },
    {
      args: [
        ['wfhrtmiklogu', 'aaucxswi', 'kiuazndjyj', 'djgtneursmcay', 'yyogmdlik', 'ivkztqic', 'bdbfauygphg', 'kihipefzt', 'dxblxxldbr', 'eyzeyllyrp', 'ygolfmu', 'owlpcmcmubn', 'bzsvvola', 'adbldcgugry', 'rapdfvxrbt', 'fpyvycpjmoogc', 'iqtwnim', 'ybeduakpmiuu', 'kpddyjmf', 'woyissfnqb', 'lbiqriwekm', 'saulyuzefov', 'ygcexximwy', 'tjbjtphj', 'dpwuxrt', 'vliqzyswy', 'fphlmal', 'eoacuiewubyi', 'mwqtigfoqpgf', 'oesosylghg', 'ossebwjjget', 'znpiart', 'jnevgey', 'cdkcnakjgrzkj', 'auqgveeh', 'ewwwxlbbfhmjq', 'xihowrz', 'ompflyixnnlo', 'xvgbotvyjlnlqnlv', 'qswyswbecwqw', 'lzvlfacfkdhl', 'bnlvwwtrsfmugn', 'ptbgtszdcdjphfll', 'xxgyhqe', 'amrypf', 'lpcuiqn', 'wuggtogdxjg', 'qihtlajqsjmcaxy', 'mevaseuduqtms', 'oalsoolbawht', 'vxxyytnjb', 'emhivcippoeswabvdy', 'vvfirylrnigx', 'hwhcannpwznke', 'fjhtialujmurlmii', 'jhybyfebt', 'klmsyniadn', 'frtpybdj', 'veqvofowgohr', 'hjfqarsfl', 'twjkeyiyowrsg', 'kqnedettqcy', 'oejbyt', 'jcpzecrnvsb', 'jdmdmkbejmt', 'ksyidrgxznb', 'piggefpxwn', 'ucbxkdfjzsu', 'frsliattnwr', 'kwapudgi', 'gceljtaffwoj', 'bvxmkzys', 'opikssq', 'mwicafgxm', 'gercizrb', 'dpbgoairma', 'ntgsdicreabogrslnv', 'crrcazyiw', 'aeaslbsxtkxcgbe', 'rwsevs', 'pzbcqmwazvoz', 'wzntybjlx', 'agtrhxjglq', 'cunifqxbqa', 'utvlpmqfxxca', 'vvqtyqhp', 'emxtxzoafbrtrf', 'nlofycznx', 'tzjairttxf', 'glafsryguspl', 'kwzybzb', 'hxjypiflk', 'bnadghtm', 'lmgxznmczii', 'shgokyzvvxzny', 'hiuihrjs', 'lnmvviozrnz', 'neieutbc', 'rllvfxansltt', 'dfqoycu', 'ynbwjhwe', 'kadjuirviy', 'qnzbodlsv', 'fepyhlhtdehtyjv', 'mkscjcsqwblqy', 'rxbgewtfoqvp', 'kbgxrfznzpbl', 'kfsbehwtcdbkh', 'qpvmtitcohy', 'zpfohgt', 'vsqbedpsu', 'bycujpjfvzc', 'hvltzdutz', 'vdngqlkvv', 'janitwexup', 'loxgcyzk', 'jdnvubpmsro', 'apwwwjivagexidk', 'tjjxpmadtxd', 'nexqckpntst', 'wwmrwxaxrgb', 'hzvyeozd', 'yfvrtdzzag', 'qzmiunovryox', 'swsliglenutp', 'ptpnfvobgz', 'njlkervql', 'xplthkgun', 'urclbfivx', 'aouyob', 'weofteoqyo', 'kwbzaietac', 'ymqqqqknmbrjk', 'anugiiccx', 'xsctycidfgwtxi', 'oauakqjyjzhjas', 'shqodtjujh', 'ltfpvxrhbr', 'cexisilokkmr', 'jojdsawjc', 'dfidinng', 'fukuzflutph', 'qenskzwyqnp', 'makwqibzx', 'mfuejtjlffsj', 'tjncyvwnxvun', 'akvljthycrtg', 'awjtxkzdwdj', 'kmajjofwqy', 'rwdakjue', 'lcclpfaq', 'evcdrvnyzc', 'weenlfjol', 'wsfnihuzpr', 'lrseedyg', 'mlnowtdgldbrnzp', 'ijsqcdz', 'ykflkbmkqzlyq', 'oaxizzl'],
        76
      ],
      expected: 'cdkcnakjgrzkjauqgveehewwwxlbbfhmjqxihowrzompflyixnnloxvgbotvyjlnlqnlvqswyswbecwqwlzvlfacfkdhlbnlvwwtrsfmugnptbgtszdcdjphfllxxgyhqeamrypflpcuiqnwuggtogdxjgqihtlajqsjmcaxymevaseuduqtmsoalsoolbawhtvxxyytnjbemhivcippoeswabvdyvvfirylrnigxhwhcannpwznkefjhtialujmurlmiijhybyfebtklmsyniadnfrtpybdjveqvofowgohrhjfqarsfltwjkeyiyowrsgkqnedettqcyoejbytjcpzecrnvsbjdmdmkbejmtksyidrgxznbpiggefpxwnucbxkdfjzsufrsliattnwrkwapudgigceljtaffwojbvxmkzysopikssqmwicafgxmgercizrbdpbgoairmantgsdicreabogrslnvcrrcazyiwaeaslbsxtkxcgberwsevspzbcqmwazvozwzntybjlxagtrhxjglqcunifqxbqautvlpmqfxxcavvqtyqhpemxtxzoafbrtrfnlofycznxtzjairttxfglafsrygusplkwzybzbhxjypiflkbnadghtmlmgxznmcziishgokyzvvxznyhiuihrjslnmvviozrnzneieutbcrllvfxanslttdfqoycuynbwjhwekadjuirviyqnzbodlsvfepyhlhtdehtyjvmkscjcsqwblqyrxbgewtfoqvpkbgxrfznzpblkfsbehwtcdbkhqpvmtitcohy'
    },
    {
      args: [
        ['tklvnuarvwt', 'eqhxqrfgf', 'yukjxegvoszh', 'xdbuyfipva', 'kiveipyik', 'jhukedy', 'mvfzutcrnbdd', 'voiela', 'llzptlyoke', 'seeggibqrjv', 'fhdqztgmpsn', 'kyahfnppesd', 'nyrcyjhcxaz', 'tjdqmmfze', 'eyrenxnxkjam', 'ebxqlwvmhkhitbv', 'vxcxolvvov', 'tccniknz', 'cfibayimbpizyw', 'tntjmchmqrmvvvp', 'nbgbnmjdhlt', 'sxnulbbwjb', 'glvxoib', 'fprbhiffxgesagr', 'spdwnabdb', 'qzaqqgjw', 'wftejkeflpzm', 'utxpkwjhgspoa', 'zrmzvdq', 'vbqmanfqlalj', 'xinavtxrkemvkbb', 'sbrdbppdfm', 'cvandnnbjlqn', 'vwhnptujvn', 'rypfrfla', 'uezvrzchfy', 'cbpiochwqgi', 'hguucjkyqqmf', 'eojwef', 'klhgkybn', 'flojwidwvdb', 'hewoixwjosqb', 'pyimxjhxep', 'fthffkheajoe', 'fykwllf', 'kageixjvdyhbzedo', 'ceamhkaulpdz', 'mwfaayhojccq', 'pajaaxdwy', 'gcyfwzttsk', 'jxtbvjerevwy', 'dwugvfe', 'pudnkerm', 'ndthnonlsqetivpcxc', 'reiidemot', 'npppbelfkwzbl', 'hbbwhnrf', 'wupfygytecd', 'munpsoib', 'opvucjfhhpraeqhj', 'btnqogmzuge', 'jrzekyrftgkjkkxb', 'umxtnlenbgy', 'imatphklsa', 'xwomjivnwsyuobi', 'jdopxrlefafdl', 'qorsuinecjkby', 'raptzopsrdto', 'viuxpvchol', 'lhfswraicigeoxc', 'jjdxkebdshdoobqy', 'fztqumxyvfdlgevni', 'qnvsfvdkf', 'xeznfagbkasxa', 'htrszduqsrd', 'vxagvadxwhv', 'zatwdu', 'ghvmnb', 'btlxeopopg', 'rbqxbuwqvmsfs', 'dddcccmnqvf', 'turkmmb', 'qbjxpioqleq', 'kdcjkyidzedjbsf', 'jpfelgjea', 'mfaszayedkmfcgmcu', 'ragfvin', 'gpjrchnzrzndtp', 'thwnqbr', 'aonimrmrgw', 'vqeyhandb', 'skebwdxdob', 'fzybgvnbmybep', 'wkgziunmc', 'oefoczwz', 'jlvqntocob', 'znckljxedjht', 'faegtpfi', 'rzpwlxojptjug', 'xgyroys', 'ikngxnlvioyymo', 'dsthkbjrah', 'pxqyjswhq', 'alrxqonhxuaky', 'rdvfxsjim', 'smvlvxkdw', 'wdcmphsxih', 'hfljwryfrevwjidoe', 'dcwmhwu', 'pctrpemc', 'lyyvuhthp', 'mbldymtdx', 'sjfevsiaxrovk', 'oslbchnnd', 'msjyhbuzzeriolo', 'uqcmsoeyfmu', 'nwzyle', 'mttwukbuvkzyi', 'hcjbyxmjvqjp', 'clqdjj', 'izknsjowhphx', 'tftnjie', 'yqbkzddsyyaua', 'uqjijygsdi', 'mtunbflyrtiha', 'uiwyicuxct', 'popzuqle', 'ftwcuhgx', 'gffmmmsbxcp', 'tqqlmstwqjun', 'vltovao', 'hdxnpbctte', 'fyincqks', 'hhdyiirerb', 'gzkzcztrg', 'dnfxgzm', 'bncqewpnvf', 'atmsnnrdencafep', 'zzhtdlkxkh', 'pemmounb', 'gwljnme', 'ijwrkujly', 'vvgmthnlk', 'vaedkeys', 'aswcdokzrbjyhlo', 'yquwxqnvbbv', 'opzjod', 'eangufevzomkc', 'ulfgjqjgvtz', 'kbajduqtx', 'jvjytwdsptdsz', 'eutrucceivwii', 'rujmcuxpifww', 'ydoochetirh', 'icqzcu', 'waqusixet', 'tzgxneqmsop', 'qnsbrkobzb', 'rzmcahhoupfzxaro', 'aoiymtego', 'qmehfsmrwiz', 'slqrjxniyow', 'nfvfhmyofo', 'nediktqp', 'ylvdnaopbiqa', 'juejkparrrzm', 'ikjjqfmgiwo', 'zncbnmwg', 'mufubd', 'bdrmzr', 'jjdwqkorb', 'zyhwbezth', 'oxtdlfwssa', 'qntypxry'],
        138
      ],
      expected: 'eyrenxnxkjamebxqlwvmhkhitbvvxcxolvvovtccniknzcfibayimbpizywtntjmchmqrmvvvpnbgbnmjdhltsxnulbbwjbglvxoibfprbhiffxgesagrspdwnabdbqzaqqgjwwftejkeflpzmutxpkwjhgspoazrmzvdqvbqmanfqlaljxinavtxrkemvkbbsbrdbppdfmcvandnnbjlqnvwhnptujvnrypfrflauezvrzchfycbpiochwqgihguucjkyqqmfeojwefklhgkybnflojwidwvdbhewoixwjosqbpyimxjhxepfthffkheajoefykwllfkageixjvdyhbzedoceamhkaulpdzmwfaayhojccqpajaaxdwygcyfwzttskjxtbvjerevwydwugvfepudnkermndthnonlsqetivpcxcreiidemotnpppbelfkwzblhbbwhnrfwupfygytecdmunpsoibopvucjfhhpraeqhjbtnqogmzugejrzekyrftgkjkkxbumxtnlenbgyimatphklsaxwomjivnwsyuobijdopxrlefafdlqorsuinecjkbyraptzopsrdtoviuxpvchollhfswraicigeoxcjjdxkebdshdoobqyfztqumxyvfdlgevniqnvsfvdkfxeznfagbkasxahtrszduqsrdvxagvadxwhvzatwdughvmnbbtlxeopopgrbqxbuwqvmsfsdddcccmnqvfturkmmbqbjxpioqleqkdcjkyidzedjbsfjpfelgjeamfaszayedkmfcgmcuragfvingpjrchnzrzndtpthwnqbraonimrmrgwvqeyhandbskebwdxdobfzybgvnbmybepwkgziunmcoefoczwzjlvqntocobznckljxedjhtfaegtpfirzpwlxojptjugxgyroysikngxnlvioyymodsthkbjrahpxqyjswhqalrxqonhxuakyrdvfxsjimsmvlvxkdwwdcmphsxihhfljwryfrevwjidoedcwmhwupctrpemclyyvuhthpmbldymtdxsjfevsiaxrovkoslbchnndmsjyhbuzzeriolouqcmsoeyfmunwzylemttwukbuvkzyihcjbyxmjvqjpclqdjjizknsjowhphxtftnjieyqbkzddsyyauauqjijygsdimtunbflyrtihauiwyicuxctpopzuqleftwcuhgxgffmmmsbxcptqqlmstwqjunvltovaohdxnpbcttefyincqkshhdyiirerbgzkzcztrgdnfxgzmbncqewpnvfatmsnnrdencafepzzhtdlkxkhpemmounbgwljnmeijwrkujlyvvgmthnlkvaedkeysaswcdokzrbjyhloyquwxqnvbbvopzjodeangufevzomkculfgjqjgvtzkbajduqtxjvjytwdsptdszeutrucceivwii'
    }
  ]

  cases.forEach(function (c) {
    test(`should return the longest string formed by concatenating ${c.args[1]} consecutive strings from the array ${c.args[0]}`, function () {
      var res = kata.longestConsec.apply(null, c.args)
      expect(res).toBe(c.expected)
    })
  })
})

describe('tickets()', function () {
  var cases = [
    {args: [[25, 25, 50]], expected: 'YES'},
    {args: [[25, 100]], expected: 'NO'},
    {args: [[25, 25, 50, 100, 25, 25, 25, 100, 25, 25, 25, 100, 25, 25, 50, 100]], expected: 'YES'},
    {args: [[25, 25, 25, 25, 25, 25, 25, 50, 50, 50, 100, 100, 100, 100]], expected: 'NO'},
    {args: [[25, 25, 25, 25, 25, 25, 25, 50, 50, 50, 100, 100, 100]], expected: 'YES'}
  ]

  cases.forEach(function (c) {
    test(`should check if Vasya the clerk has enough change for the line of people ${c.args[0]} `, function () {
      var res = kata.tickets.apply(null, c.args)
      expect(res).toBe(c.expected)
    })
  })
})

describe('descendingOrder()', function () {
  var cases = [
    {args: [21445], expected: 54421},
    {args: [145263], expected: 654321},
    {args: [123456789], expected: 987654321},
    {args: [1254859723], expected: 9875543221}
  ]

  cases.forEach(function (c) {
    test(`should sort the digits in ${c.args[0]} in descending order`, function () {
      var res = kata.descendingOrder.apply(null, c.args)
      expect(res).toBe(c.expected)
    })
  })
})

describe('filterList()', function () {
  var cases = [
    {args: [[1, 2, 'a', 'b']], expected: [1, 2]},
    {args: [[1, 'a', 'b', 0, 15]], expected: [1, 0, 15]},
    {args: [[1, 2, 'aasf', '1', '123', 123]], expected: [1, 2, 123]}
  ]

  cases.forEach(function (c) {
    test(`should filter out all the non-number elements in ${c.args[0]}`, function () {
      var res = kata.filterList.apply(null, c.args)
      expect(res).toEqual(c.expected)
    })
  })
})

describe('linkedLists', function () {
  let Node = kata.linkedListsPushBuild.Node
  let push = kata.linkedListsPushBuild.push
  let build = kata.linkedListsPushBuild.build
  let buildOneTwoThree = kata.linkedListsPushBuild.buildOneTwoThree
  let length = kata.linkedListsLengthCount.length
  let count = kata.linkedListsLengthCount.count
  let getNth = kata.linkedListsGetNthNode
  let sortedInsert = kata.linkedListsSortedInsert
  let append = kata.linkedListsAppend
  let removeDuplicates = kata.linkedListsRemoveDuplicates
  let moveNode = kata.linkedListsMoveNode
  let moveNodeInPlace = kata.linkedListsMoveNodeInPlace
  let alternatingSplit =
    kata.linkedListsAlternatingSplit.alternatingSplit
  let SplitContext = kata.linkedListsAlternatingSplit.Context
  let frontBackSplit = kata.linkedListsFrontBackSplit
  let shuffleMerge = kata.linkedListsShuffleMerge
  let sortedMerge = kata.linkedListsSortedMerge
  let mergeSort = kata.linkedListsMergeSort
  let sortedIntersect = kata.linkedListsSortedIntersect
  let iterativeReverse = kata.linkedListsIterativeReverse
  let recursiveReverse = kata.linkedListsRecursiveReverse

  // Helpers
  let buildListFromArray = (arr) => build(...(arr.reverse()))
  function printList (list) { // eslint-disable-line no-unused-vars
    let current = list
    let result = ''
    while (current) {
      result += current.data + ' ->'
      current = current.next
    }
    result += 'null'
    console.log(result)
  }
  function listEquals (actual, expected) {
    if (!actual && !expected) return
    else if ((actual && !expected) || (!actual && expected)) {
      fail("Lists aren't equal")
      return
    } else if ((actual.data === null && expected.data) ||
              (actual.data && expected.data == null)) {
      fail("Lists aren't equal")
      return
    }

    let currentA = actual
    let currentE = expected
    while (currentA !== null && currentE !== null) {
      expect(currentA.data).toBe(currentE.data)
      currentA = currentA.next
      currentE = currentE.next
    }
    if ((currentA && !currentE) || (!currentA && currentE)) {
      fail("Lists aren't equal")
    }
  }

  describe('linkedListsPushBuild()', function () {
    let value1 = 1
    let value2 = 2
    let value3 = 3

    describe('tests push()', function () {
      test('should insert a node in an empty list.', function () {
        let chained = push(null, value1)
        expect(chained.data).toBe(value1)
        expect(chained.next).toBeNull()
      })

      test('should insert a node before another node.', function () {
        let chained = push(new Node(value1), value2)
        expect(chained.data).toBe(value2)
        expect(chained.next.data).toBe(value1)
      })
    })
    describe('tests buildOneTwoThree()', function () {
      test('should build a list with three elements', function () {
        let chained = buildOneTwoThree()
        expect(chained.data).toBe(value1)
        expect(chained.next.data).toBe(value2)
        expect(chained.next.next.data).toBe(value3)
        expect(chained.next.next.next).toBeNull()
      })
    })
  })

  describe('linkedListLengthCount()', function () {
    describe('tests length()', function () {
      let listLengths = [10, 100, 1000]
      listLengths.forEach(function (listLength) {
        let chained = push(null, 1)
        for (let i = 0; i < (listLength - 1); i++) { chained = push(chained, i) }
        test(`should return length of a ${listLength} element list`, function () {
          expect(length(chained)).toBe(listLength)
        })
      })
    })

    describe('tests count()', function () {
      let frequency = {
        0: [3, 7, 1, 0, 5],
        1: [7, 3, 9, 10, 5]
      }
      let chained, zeroCount, oneCount

      for (let i = 0; i < frequency[0].length; i++) {
        zeroCount = frequency[0][i]
        oneCount = frequency[1][i]

        test(`should count ${zeroCount} zeros and ${oneCount} ones`, function () {
          if (zeroCount > 0) {
            chained = push(null, 0)
          } else {
            chained = push(null, 1)
          }
          for (let j = 0; j < zeroCount - 1; j++) {
            chained = push(chained, 0)
          }
          for (let j = 0;
                j < (zeroCount === 0 ? oneCount - 1 : oneCount);
                j++) {
            chained = push(chained, 1)
          }
          expect(count(chained, 0)).toBe(zeroCount)
          expect(count(chained, 1)).toBe(oneCount)
        })
      }
    })
  })
  describe('linkedListsGetNthNode()', function () {
    let trialCount = 10
    let longTestSuiteName
    let longTestName

    test('should get first node of list of length 1', function () {
      let targetVal = 1
      let index = 0
      let chained = push(null, targetVal)
      expect(getNth(chained, index).data).toBe(targetVal)
    })
    test('should get first node of list of length 3', function () {
      let targetVal = 1
      let index = 0
      let chained = buildOneTwoThree()
      expect(getNth(chained, index).data).toBe(targetVal)
    })
    test('should get last node of list of length 3', function () {
      let targetVal = 3
      let index = 2
      let chained = buildOneTwoThree()
      expect(getNth(chained, index).data).toBe(targetVal)
    })
    longTestName = 'should throw an exception when accessing an ' +
                   'element one index beyond the list end'
    test(longTestName, function () {
      let index = 3
      let chained = buildOneTwoThree()
      expect(() => getNth(chained, index)).toThrow()
    })
    longTestName = 'should throw an exception when accessing index an ' +
                   'element multiple indicies beyond the list end'
    test(longTestName, function () {
      let index = 10
      let chained = buildOneTwoThree()
      expect(() => getNth(chained, index)).toThrow()
    })
    longTestSuiteName = `should access ${trialCount} random indices ` +
                        'of a large list'
    describe(longTestSuiteName, function () {
      let chained
      let listSize = 1000
      chained = push(null, listSize - 1)
      for (let i = listSize - 2; i >= 0; i--) {
        chained = push(chained, i)
      }
      for (let i = 0; i < trialCount; i++) {
        let index = Math.floor(Math.random() * listSize)
        longTestName = `should access index ${index} of a ${listSize} ` +
                       'element list'
        test(longTestName, function () {
          expect(getNth(chained, index).data).toBe(index)
        })
      }
    })
  })

  describe('linkedListsSortedInsert()', function () {
    let longTestName
    let listBeforeInsertion
    let insertedValue
    let expectedList
    let actualList

    longTestName = 'should insert at end of a small list according ' +
                   'element order'
    test(longTestName, function () {
      insertedValue = 4
      listBeforeInsertion = buildListFromArray([1, 2, 3])
      expectedList = buildListFromArray([1, 2, 3, 4])

      actualList = sortedInsert(listBeforeInsertion, insertedValue)
      listEquals(actualList, expectedList)
    })

    longTestName = 'should insert at beginning of a small list ' +
                   'according element order'
    test(longTestName, function () {
      insertedValue = 1
      listBeforeInsertion = buildListFromArray([4, 7, 12])
      expectedList = buildListFromArray([1, 4, 7, 12])

      actualList = sortedInsert(listBeforeInsertion, insertedValue)
      listEquals(actualList, expectedList)
    })

    longTestName = 'should insert between two elements of a small list ' +
                   '(not at index 0, -1)'
    test(longTestName, function () {
      insertedValue = 5
      listBeforeInsertion = buildListFromArray([1, 7, 8])
      expectedList = buildListFromArray([1, 5, 7, 8])

      actualList = sortedInsert(listBeforeInsertion, insertedValue)
      listEquals(actualList, expectedList)
    })

    test('should insert into an empty list that is null', function () {
      insertedValue = 5
      listBeforeInsertion = null
      expectedList = new Node(insertedValue)

      actualList = sortedInsert(listBeforeInsertion, insertedValue)
      listEquals(actualList, expectedList)
    })
  })

  describe('linkedListsAppend()', function () {
    let longTestName
    let listA
    let listB
    let expectedList
    let actualList

    longTestName = 'should append two lists each containing one ' +
                   'element each'
    test(longTestName, function () {
      listA = buildListFromArray([1])
      listB = buildListFromArray([2])
      expectedList = buildListFromArray([1, 2])

      actualList = append(listA, listB)
      listEquals(actualList, expectedList)
    })

    test('should append when listA is null', function () {
      listB = buildListFromArray([1, 2, 3])

      actualList = append(null, listB)
      listEquals(actualList, listB)
    })

    test('should append when listB is null', function () {
      listA = buildListFromArray([1, 2, 3])

      actualList = append(listA, null)
      listEquals(actualList, listA)
    })

    test('should append two large lists', function () {
      let arraySize = 100
      let expectedArraySize = arraySize * 2
      let arrayA = Array(arraySize)
      let arrayB = Array(arraySize)
      let expectedArray = Array(expectedArraySize)

      for (let i = 0; i < arraySize; i++) {
        arrayA[i] = expectedArray[i] = i
        arrayB[i] = expectedArray[arraySize + i] = arraySize - i
      }

      listA = buildListFromArray(arrayA)
      listB = buildListFromArray(arrayB)
      expectedList = buildListFromArray(expectedArray)

      actualList = append(listA, listB)
      listEquals(actualList, expectedList)
    })
  })

  describe('linkedListsRemoveDuplicates()', function () {
    let longTestName
    let inputList
    let expectedList
    let actualList

    test('should remove one repeat from center of list', function () {
      inputList = buildListFromArray([1, 2, 2, 3])
      expectedList = buildListFromArray([1, 2, 3])

      actualList = removeDuplicates(inputList)
      listEquals(actualList, expectedList)
    })

    test('should remove one repeat at first list index', function () {
      inputList = buildListFromArray([1, 1, 2, 3])
      expectedList = buildListFromArray([1, 2, 3])

      actualList = removeDuplicates(inputList)
      listEquals(actualList, expectedList)
    })

    test('should remove one repeat at last list index', function () {
      inputList = buildListFromArray([1, 2, 3, 3])
      expectedList = buildListFromArray([1, 2, 3])

      actualList = removeDuplicates(inputList)
      listEquals(actualList, expectedList)
    })

    longTestName = 'should remove several repeats scattered around ' +
                   'the list center'
    test(longTestName, function () {
      let inputArray = [1, 2, 2, 3, 4, 4, 4, 5, 6, 6, 6, 6, 7]
      inputList = buildListFromArray(inputArray)
      expectedList = buildListFromArray([1, 2, 3, 4, 5, 6, 7])

      actualList = removeDuplicates(inputList)
      listEquals(actualList, expectedList)
    })
  })

  describe('linkedListsMoveNode()', function () {
    let longTestName
    let sourceList
    let destList
    let expectedSource
    let expectedDest
    let actualLists

    test('should handle two multi-node lists', function () {
      sourceList = buildListFromArray([1, 2, 2])
      destList = buildListFromArray([3, 4, 5])
      expectedSource = buildListFromArray([2, 2])
      expectedDest = buildListFromArray([1, 3, 4, 5])

      actualLists = moveNode(sourceList, destList)
      listEquals(actualLists.source, expectedSource)
      listEquals(actualLists.dest, expectedDest)
    })

    longTestName = 'should handle a multi-node source list and a ' +
                   'single-node dest list'
    test(longTestName, function () {
      sourceList = buildListFromArray([1, 2, 2])
      destList = buildListFromArray([3])
      expectedSource = buildListFromArray([2, 2])
      expectedDest = buildListFromArray([1, 3])

      actualLists = moveNode(sourceList, destList)
      listEquals(actualLists.source, expectedSource)
      listEquals(actualLists.dest, expectedDest)
    })

    longTestName = 'should handle a multi-node source list and a ' +
                   'null dest list'
    test(longTestName, function () {
      sourceList = buildListFromArray([1, 2, 2])
      destList = null
      expectedSource = buildListFromArray([2, 2])
      expectedDest = buildListFromArray([1])

      actualLists = moveNode(sourceList, destList)
      listEquals(actualLists.source, expectedSource)
      listEquals(actualLists.dest, expectedDest)
    })

    longTestName = 'should handle a single-node source list and a ' +
                    'multi-node dest list'
    test(longTestName, function () {
      sourceList = buildListFromArray([1])
      destList = buildListFromArray([3, 4, 5])
      expectedSource = null
      expectedDest = buildListFromArray([1, 3, 4, 5])

      actualLists = moveNode(sourceList, destList)
      listEquals(actualLists.source, expectedSource)
      listEquals(actualLists.dest, expectedDest)
    })

    test('should handle a null source list', function () {
      sourceList = null
      destList = buildListFromArray([3, 4, 5])
      expect(() => moveNode(sourceList, destList)).toThrow('No source node available for moving')
    })
  })

  describe('linkedListsMoveNodeInPlace()', function () {
    let longTestName
    let actualSource
    let actualDest
    let expectedSource
    let expectedDest

    test('should handle two multi-node lists', function () {
      actualSource = buildListFromArray([1, 2, 2])
      actualDest = buildListFromArray([3, 4, 5])
      expectedSource = buildListFromArray([2, 2])
      expectedDest = buildListFromArray([1, 3, 4, 5])

      moveNodeInPlace(actualSource, actualDest)
      listEquals(actualSource, expectedSource)
      listEquals(actualDest, expectedDest)
    })

    test('should handle two multi-node lists repeatedly', function () {
      actualSource = buildListFromArray([1, 2, 3])
      actualDest = buildListFromArray([1, 2, 3])

      moveNodeInPlace(actualSource, actualDest)
      listEquals(actualSource, buildListFromArray([2, 3]))
      listEquals(actualDest, buildListFromArray([1, 1, 2, 3]))

      moveNodeInPlace(actualSource, actualDest)
      listEquals(actualSource, buildListFromArray([3]))
      listEquals(actualDest, buildListFromArray([2, 1, 1, 2, 3]))

      moveNodeInPlace(actualSource, actualDest)
      listEquals(actualSource, new Node(null))
      listEquals(actualDest, buildListFromArray([3, 2, 1, 1, 2, 3]))

      expect(() => moveNodeInPlace(actualSource, actualDest)).toThrow()
    })

    longTestName = 'should handle a multi-node source list and a ' +
                   'single-node dest list'
    test(longTestName, function () {
      actualSource = buildListFromArray([1, 2, 2])
      actualDest = buildListFromArray([3])
      expectedSource = buildListFromArray([2, 2])
      expectedDest = buildListFromArray([1, 3])

      moveNodeInPlace(actualSource, actualDest)
      listEquals(actualSource, expectedSource)
      listEquals(actualDest, expectedDest)
    })

    longTestName = 'should handle a multi-node source list and an ' +
                   'empty dest list'
    test(longTestName, function () {
      actualSource = buildListFromArray([1, 2, 2])
      actualDest = new Node(null, null)
      expectedSource = buildListFromArray([2, 2])
      expectedDest = buildListFromArray([1])

      moveNodeInPlace(actualSource, actualDest)

      listEquals(actualSource, expectedSource)
      listEquals(actualDest, expectedDest)
    })

    test('should handle a null dest list', function () {
      actualSource = buildListFromArray([1, 2, 2])
      actualDest = null
      expect(() => moveNodeInPlace(actualSource, actualDest)).toThrow()
    })

    longTestName = 'should handle single-node source list and a ' +
                    'multi-node dest list'
    test(longTestName, function () {
      actualSource = buildListFromArray([1])
      actualDest = buildListFromArray([3, 4, 5])
      expectedSource = new Node(null, null)
      expectedDest = buildListFromArray([1, 3, 4, 5])

      moveNodeInPlace(actualSource, actualDest)

      listEquals(actualSource, expectedSource)
      listEquals(actualDest, expectedDest)
    })

    longTestName = 'should handle a null source list'
    test(longTestName, function () {
      actualSource = null
      actualDest = buildListFromArray([3, 4, 5])
      expect(() => moveNodeInPlace(actualSource, actualDest)).toThrow()
    })

    longTestName = 'should handle a empty source list'
    test(longTestName, function () {
      actualSource = new Node(null, null)
      actualDest = buildListFromArray([3, 4, 5])
      expect(() => moveNodeInPlace(actualSource, actualDest)).toThrow()
    })
  })

  describe('linkedListsAlternatingSplit()', function () {
    let input
    let expectedContext = new SplitContext()
    let actualContext

    test('should handle bad input', function () {
      expect(() => alternatingSplit(null)).toThrow()
      expect(() => alternatingSplit(buildListFromArray([1]))).toThrow()
    })

    test('should handle an input of 5 elements', function () {
      input = buildListFromArray([0, 1, 0, 1, 0])
      expectedContext.first = buildListFromArray([0, 0, 0])
      expectedContext.second = buildListFromArray([1, 1])

      actualContext = alternatingSplit(input)
      listEquals(actualContext.first, expectedContext.first)
      listEquals(actualContext.second, expectedContext.second)
    })

    test('should handle an input of 6 elements', function () {
      input = buildListFromArray([0, 1, 0, 1, 0, 1])
      expectedContext.first = buildListFromArray([0, 0, 0])
      expectedContext.second = buildListFromArray([1, 1, 1])

      actualContext = alternatingSplit(input)
      listEquals(actualContext.first, expectedContext.first)
      listEquals(actualContext.second, expectedContext.second)
    })
  })

  describe('linkedListsFrontBackSplit()', function () {
    let source
    let front
    let back
    let expectedFront
    let expectedBack

    test('should handle bad input', function () {
      front = new Node()
      back = new Node()
      source = buildListFromArray([1])
      expect(() => frontBackSplit(null, front, back)).toThrow()
      expect(() => frontBackSplit(source, front, back)).toThrow()
    })

    test('should handle an input of 2 elements', function () {
      source = buildListFromArray([0, 1])

      expectedFront = buildListFromArray([0])
      expectedBack = buildListFromArray([1])
      front = new Node()
      back = new Node()

      frontBackSplit(source, front, back)

      listEquals(front, expectedFront)
      listEquals(back, expectedBack)
    })

    test('should handle an input of 5 elements', function () {
      source = buildListFromArray([0, 0, 0, 1, 1])

      expectedFront = buildListFromArray([0, 0, 0])
      expectedBack = buildListFromArray([1, 1])
      front = new Node()
      back = new Node()

      frontBackSplit(source, front, back)

      listEquals(front, expectedFront)
      listEquals(back, expectedBack)
    })

    test('should handle an input of 7 elements', function () {
      source = buildListFromArray([0, 0, 0, 0, 1, 1, 1])

      expectedFront = buildListFromArray([0, 0, 0, 0])
      expectedBack = buildListFromArray([1, 1, 1])
      front = new Node()
      back = new Node()

      frontBackSplit(source, front, back)

      listEquals(front, expectedFront)
      listEquals(back, expectedBack)
    })

    test('should handle an input of 6 elements', function () {
      source = buildListFromArray([0, 0, 0, 1, 1, 1])

      expectedFront = buildListFromArray([0, 0, 0])
      expectedBack = buildListFromArray([1, 1, 1])
      front = new Node()
      back = new Node()

      frontBackSplit(source, front, back)

      listEquals(front, expectedFront)
      listEquals(back, expectedBack)
    })

    test('should handle an input of 8 elements', function () {
      source = buildListFromArray([0, 0, 0, 0, 1, 1, 1, 1])

      expectedFront = buildListFromArray([0, 0, 0, 0])
      expectedBack = buildListFromArray([1, 1, 1, 1])
      front = new Node()
      back = new Node()

      frontBackSplit(source, front, back)

      listEquals(front, expectedFront)
      listEquals(back, expectedBack)
    })
  })

  describe('linkedListsShuffleMerge()', function () {
    let first
    let second
    let actual
    let expected
    let input

    test('should handle do-nothing input', function () {
      input = buildListFromArray([1, 2, 3])
      actual = shuffleMerge(null, input)
      listEquals(actual, input)

      actual = shuffleMerge(input, null)
      listEquals(actual, input)
    })

    describe('should handle small inputs of same length', function () {
      test('where lengths = 1', function () {
        first = buildListFromArray([1])
        second = buildListFromArray([2])
        expected = buildListFromArray([1, 2])

        actual = shuffleMerge(first, second)

        listEquals(actual, expected)
      })

      test('where lengths = 2', function () {
        first = buildListFromArray([1, 3])
        second = buildListFromArray([2, 4])
        expected = buildListFromArray([1, 2, 3, 4])

        actual = shuffleMerge(first, second)

        listEquals(actual, expected)
      })

      test('where lengths = 3', function () {
        first = buildListFromArray([1, 3, 5])
        second = buildListFromArray([2, 4, 6])
        expected = buildListFromArray([1, 2, 3, 4, 5, 6])

        actual = shuffleMerge(first, second)

        listEquals(actual, expected)
      })
    })

    describe('should handle a shorter 1st list', function () {
      test('where length difference = 1', function () {
        first = buildListFromArray([1, 3])
        second = buildListFromArray([2, 4, 6])
        expected = buildListFromArray([1, 2, 3, 4, 6])

        actual = shuffleMerge(first, second)

        listEquals(actual, expected)
      })

      test('where length difference = 2', function () {
        first = buildListFromArray([1])
        second = buildListFromArray([2, 4, 6])
        expected = buildListFromArray([1, 2, 4, 6])

        actual = shuffleMerge(first, second)

        listEquals(actual, expected)
      })
    })

    describe('should handle a shorter 2nd list', function () {
      test('where length difference = 1', function () {
        first = buildListFromArray([1, 3, 5])
        second = buildListFromArray([2, 4])
        expected = buildListFromArray([1, 2, 3, 4, 5])

        actual = shuffleMerge(first, second)

        listEquals(actual, expected)
      })

      test('where length difference = 2', function () {
        first = buildListFromArray([1, 3, 5])
        second = buildListFromArray([2])
        expected = buildListFromArray([1, 2, 3, 5])

        actual = shuffleMerge(first, second)
        listEquals(actual, expected)
      })
    })
  })

  describe('linkedListsSortedMerge()', function () {
    let first
    let second
    let actual
    let expected
    let input

    test('should handle do-nothing input', function () {
      input = buildListFromArray([2, 4, 6, 7])
      actual = sortedMerge(null, input)
      listEquals(actual, input)

      actual = sortedMerge(input, null)
      listEquals(actual, input)
    })

    test('should handle short inputs', function () {
      first = buildListFromArray([2, 4, 6, 7])
      second = buildListFromArray([1, 3, 5, 6, 8])
      expected = buildListFromArray([1, 2, 3, 4, 5, 6, 6, 7, 8])

      actual = sortedMerge(first, second)
      listEquals(actual, expected)
    })
  })

  describe('linkedListsMergeSort()', function () {
    let input
    let actual
    let expected

    test('should handle a null input', function () {
      actual = mergeSort(null)
      listEquals(actual, null)
    })

    test('should handle a 1-node input', function () {
      input = buildListFromArray([1])
      actual = mergeSort(input)
      listEquals(actual, input)
    })

    test('should handle a 2-node input', function () {
      input = buildListFromArray([2, 1])
      expected = buildListFromArray([1, 2])
      actual = mergeSort(input)
      listEquals(actual, expected)
    })

    test('should handle a 3-node input', function () {
      input = buildListFromArray([3, 2, 1])
      expected = buildListFromArray([1, 2, 3])
      actual = mergeSort(input)
      listEquals(actual, expected)
    })

    test('should handle a 6-node list', function () {
      input = buildListFromArray([4, 2, 1, 3, 8, 9])
      expected = buildListFromArray([1, 2, 3, 4, 8, 9])
      actual = mergeSort(input)
      listEquals(actual, expected)
    })
  })

  describe('linkedListsSortedIntersect()', function () {
    let input
    let first
    let second
    let actual
    let expected

    test('should handle a null input', function () {
      input = buildListFromArray([1, 2, 3])
      actual = sortedIntersect(null, input)
      listEquals(actual, null)

      actual = sortedIntersect(input, null)
      listEquals(actual, null)
    })

    test('should handle inputs with multiple intersections', function () {
      first = buildListFromArray([1, 3, 5, 7, 8, 9])
      second = buildListFromArray([2, 3, 6, 7, 9])
      expected = buildListFromArray([3, 7, 9])
      actual = sortedIntersect(first, second)
      listEquals(actual, expected)
    })

    test('should handle inputs no intersections', function () {
      first = buildListFromArray([1, 2, 3, 4, 5])
      second = buildListFromArray([6, 7, 8, 9, 10])
      expected = null
      actual = sortedIntersect(first, second)
      listEquals(actual, expected)
    })

    test('should handle inputs with repeated intersections', function () {
      first = buildListFromArray([1, 3, 3, 5, 7, 8, 9])
      second = buildListFromArray([2, 3, 3, 3, 6, 7, 9])
      expected = buildListFromArray([3, 7, 9])
      actual = sortedIntersect(first, second)
      listEquals(actual, expected)
    })
  })

  describe('linkedListsIterativeReverse()', function () {
    let input
    let actual
    let expected

    test('should handle a null input', function () {
      input = null
      actual = iterativeReverse(input)
      listEquals(actual, input)
    })

    test('should handle a 1-node input', function () {
      input = buildListFromArray([1])
      actual = iterativeReverse(input)
      listEquals(actual, input)
    })

    test('should handle a 2-node input', function () {
      input = buildListFromArray([0, 1])
      expected = buildListFromArray([1, 0])
      actual = iterativeReverse(input)
      listEquals(actual, expected)
    })

    test('should handle a 5-node input', function () {
      input = buildListFromArray([0, 1, 2, 3, 4])
      expected = buildListFromArray([4, 3, 2, 1, 0])
      actual = iterativeReverse(input)
      listEquals(actual, expected)
    })
  })

  describe('linkedListsRecursiveReverse()', function () {
    let input
    let actual
    let expected

    test('should handle a null input', function () {
      input = null
      actual = recursiveReverse(input)
      listEquals(actual, input)
    })

    test('should handle a 1-node input', function () {
      input = buildListFromArray([1])
      actual = recursiveReverse(input)
      listEquals(actual, input)
    })

    test('should handle a 2-node input', function () {
      input = buildListFromArray([0, 1])
      expected = buildListFromArray([1, 0])
      actual = recursiveReverse(input)
      listEquals(actual, expected)
    })

    test('should handle a 5-node input', function () {
      input = buildListFromArray([0, 1, 2, 3, 4])
      expected = buildListFromArray([4, 3, 2, 1, 0])
      actual = recursiveReverse(input)
      listEquals(actual, expected)
    })
  })
})

describe('multiply()', function () {
  var cases = [
    {args: [1, 1], expected: 1},
    {args: [1, 2], expected: 2},
    {args: [2, 1], expected: 2},
    {args: [2, 1], expected: 2}
  ]

  cases.forEach(function (c) {
    test(`should multiply ${c.args[0]} and ${c.args[1]}`, function () {
      var res = kata.multiply.apply(null, c.args)
      expect(res).toBe(c.expected)
    })
  })
})
